const express = require('express')
const app = express()
const io = app.io = require('socket.io')()
const cors = require('cors')
const bodyParser = require('body-parser')

const chat = require('./chat_namespace')

app.use(cors())
app.use(bodyParser.json())

/**
 * API routes.
 */
app.get('/ping', (_req, res) => {
  res.json('pong')
})

/**
 * Chat socket namespace
 */
chat.createNameSpace(io)

module.exports = app
