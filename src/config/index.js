
const CONFIG = {
  PORT: process.env.PORT || 3000,
  CHAT_NAMESPACE: '/socket-io',
  ORIGINS: process.env.ORIGINS || '*:*'
}

module.exports = CONFIG
