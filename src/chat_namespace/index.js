const config = require('./../config')

// socket namespace
let namespace

const onConnection = (socket) => {
  let boundUser
  console.log(`Socket connected to port ${config.PORT}`)

  socket.on('joinRoom', (content) => {
    boundUser = content
    const { user, room } = boundUser

    console.log(`user ${user.username} wants to join the room ${room}`)
    socket.join(room)
    console.log(`user ${user.username} joined the room ${room}`)
    // notify all the users in the same room
    namespace.to(room).emit('joined', user)
  })

  socket.on('publicMessage', ({ room, message }) => {
    namespace.to(room).emit('newMessage', message)
  })

  socket.on('signalingMessage', ({ candidate, description, from, room }) => {
    // broadcast rtc signaling
    namespace.to(room).emit('signalingMessage', { candidate, description, from })
  })

  socket.on('disconnect', (reason) => {
    if (boundUser) {
      const { user, room } = boundUser
      console.log(`user ${user.username} disconnected the room ${room}`)

      namespace.to(room).emit('disconnected', user)
    }
  })
}

exports.createNameSpace = (io) => {
  namespace = io
    .of(config.CHAT_NAMESPACE)
    .on('connection', onConnection)
}
